//CSIS 1400
//Midterm 1
//Ian Rolfe
//0.1b

package tryagain;

import java.awt.Dimension;

import javax.swing.JFrame;


public class Main {

    public static void main(String[] args) {

        Calculator frame = new Calculator();
        frame.setPreferredSize(new Dimension(200, 200));
        frame.pack();
        frame.setTitle("Calculate Future Investment");
        frame.setLocationRelativeTo(null); //center frame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}