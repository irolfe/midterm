package tryagain;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
 
public class InvestTable extends JFrame
{
    public InvestTable(double invest, double rate)
    {
        //headers for the table
        String[] columns = new String[] {
            "Year","Invest"
        };
         
        //actual data for the table in a 2d array
        Object[][] data = new Object[30][2]; 
        //int y = 1;		
        for(int y = 0; y < 30; y++){
        	int year = y+1;
        	double invreturn = getFutureInvestment(invest,rate,year);
        	String invstring = String.format( "%.2f", invreturn);
        	data[y][0] = year;
        	data[y][1] = invstring;
        }
        
 
        //create table with data
        JTable table = new JTable(data, columns);
         
        //add the table to the frame
        this.add(new JScrollPane(table));
         
        this.setTitle("Investment Table");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        this.pack();
        this.setVisible(true);
    }

	private double getFutureInvestment(double invest, double rate, int y) {
		int compound = 12;
		double amount = 0;		
		if(compound > 0){
			amount = invest*Math.pow(1+(rate / compound), compound*y);			
		}
		else if(compound == 0){
			amount = invest*Math.pow(Math.E, rate*y);					
		}
		return amount;
		
	}   
}