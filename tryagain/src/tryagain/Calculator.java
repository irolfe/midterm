package tryagain;


import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class Calculator extends JFrame {

    private JTextField investedAmount = new JTextField();
    private JTextField years = new JTextField();
    private JTextField annualIntRate = new JTextField();
    private JTextField future = new JTextField();

    private JButton jbtCalculate = new JButton("Calculate");

    public Calculator() {

        //panel p1 to hold labels and fields
        JPanel p1 = new JPanel(new GridLayout(4, 2));
        p1.add(new JLabel("Invested Amount"));      p1.add(this.investedAmount);
        p1.add(new JLabel("Annual Interest Rate")); p1.add(this.annualIntRate);
        p1.setBorder(new TitledBorder("Enter invested amount and interest rate"));

        //panel p2 to hold the calculate button
        JPanel p2 = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        p2.add(this.jbtCalculate);

        //add panels to frame
        add(p1, BorderLayout.CENTER);
        add(p2, BorderLayout.SOUTH);

        //register listener
        this.jbtCalculate.addActionListener(new ButtonListener());
    }

    private class ButtonListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {

            //get values from text fields
            double invest = Double.parseDouble(investedAmount.getText());
            double rate = Double.parseDouble(annualIntRate.getText());

            //display result
            
            InvestTable table;
            table = new InvestTable(invest,rate);
        }
    }
}